
$scriptblock = { 

# Resursene som skal overvåkes.
    $counters = @(
        '\PhysicalDisk(_total)\Disk Transfers/sec',
        '\Memory\Available MBytes',
        '\Processor(_Total)\% Processor Time',
        '\Process(_Total)\Working Set',
        '\Network Adapter(vmxnet3 Ethernet Adapter)\Bytes Total/sec', 
        '\USB(*)\Avg. Bytes/Transfer')


# Kode for overvåkengen.
    $getcounter = (Get-Counter -Counter $counters -MaxSamples 10 -ErrorAction SilentlyContinue) 

    $getcounter | Export-Counter -Path $home\myperflog.blg -Force; $getcounter.CounterSamples | ForEach-Object {

        [pscustomobject]@{
            TimeStamp = $_.TimeStamp
            Path      = $_.Path
            Value     = $_.CookedValue
        }  } | Export-Csv -Path $home\monitor.csv -NoTypeInformation -Force 

        get-item $home\myperflog.blg, $home\monitor.csv | Move-Item -Destination \\tafi-srv1\SF_IT-Drift -fo
    }

# Her kjøres script øverst i "Tafi-srv1" med en sikker forbinelse 
    Invoke-Command -ComputerName tafi-srv1 -ScriptBlock $scriptblock
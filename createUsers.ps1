
#	Sjekk log med Get-WinEvent at det finnes like mange log events for brukeropprettelse som antall brukere du faktisk har opprettet

## Copy-Item -Path C:\Users\Administrator\Documents\PowerShell\Scripts\mappeoppgave-2-active-directory-dcst1005\users.csv -Destination \\tafi.sec\SYSVOL\Tafi.sec\files -Recurse -Force

$u = Read-Host -Prompt 'How many users would you like to add?'

for ($i = 0 ; $i -lt $u; $i++) { 

    $FirstName = Read-Host -Prompt 'Enter First Name'
    $FirstName = $FirstName.Replace('æ','ae').Replace('Æ','AE').Replace('ø','o').Replace('Ø','O').Replace('å','aa').Replace('Å', 'AA')

    $LastName = Read-Host -Prompt 'Enter Last Name'
    $LastName = $LastName.Replace('æ','ae').Replace('Æ','AE').Replace('ø','o').Replace('Ø','O').Replace('å','aa').Replace('Å', 'AA')

    $PW = ConvertTo-SecureString "Password99" -AsPlainText -Force ## Standardpassord som alle brukere blir opprettet med (de må endre ved første pålogging)


    function Show-Menu {
        param (
            [string]$Title = 'Velg avdeling'
        )
        Clear-Host
        Write-Host "================$Title=================="
        Write-Host "1: Press '1' for Developer Team"
        Write-Host "2: Press '2' for Human Resources"
        Write-Host "3: Press '3' for IT-Drift"
        Write-Host "4: Press '4' for Regnskap"
        Write-Host "5: Press '5' for Renhold"

    }
    do {
        Show-Menu
        $selection = Read-Host "Please make a selection"
        switch ($selection) {
            '1' {
                $OU = "Developer Team"
                $sam = "developers"
            } '2' {
                $OU = "Human resources"
                $sam = "hr"
            } '3' {
                $OU = "IT-drift"
                $sam = "it.admins"
            }'4' {
                $OU = "Regnskap"
                $sam = "regnskap"
            }'5' {
                $OU = "Renhold"
                $sam = "renhold"
            }
        }
        pause
    }

    until ($selection)

    New-ADUser -Name "$FirstName $LastName" `
        -GivenName "$FirstName" `
        -Surname "$LastName" `
        -SamAccountName  ($FirstName.substring(0, 3) + $LastName.substring(0, 3)).ToLower() `
        -UserPrincipalName  "$FirstName.$LastName@tafi.sec"`
        -Path "OU=$OU,OU=Users,OU=On-PremiumIT,DC=tafi,DC=sec" `
        -AccountPassword $PW -Enabled $true `
        -ChangePasswordAtLogon 1 `
        -Department "$OU" `
 
    Add-ADGroupMember -Identity "$sam" -Members ($FirstName.substring(0, 3) + $LastName.substring(0, 3)).ToLower() 
 

    $users = Import-Csv .\users.csv
   

    $samname = ($FirstName.substring(0, 3) + $LastName.substring(0, 3)).ToLower()
    if (!($users.SamAccountName -contains $samname)) {
        Get-ADUser $samname | Select-Object Name, GivenName, Surname, SamAccountName, UserPrincipalName, DistinguishedName | export-csv -path \\tafi.sec\SYSVOL\Tafi.sec\files\users.csv -Append
        Copy-Item -Path \\tafi.sec\SYSVOL\Tafi.sec\files\users.csv -Destination C:\Users\Administrator\Documents\PowerShell\Scripts\mappeoppgave-2-active-directory-dcst1005\users.csv -Recurse -Force
    }
    
}





############## For å sjekke at log events stemmer med det faktiske antallet brukere som er oipprettet ################

$y = Read-Host -Prompt 'How many user creation logs would you like to list?'

Get-WinEvent  -MaxEvents $y -FilterHashtable `
@{ LogName='Security'; Id='4720' }  



$instalstate = (Get-WindowsFeature RSAT-AD-PowerShell ).Installstate

if ($instalstate -match "Available") {
    Install-WindowsFeature -Name RSAT-AD-PowerShell –IncludeAllSubFeature
} else {Write-Output "RSAT-AD-Powershell er allerede installert"}


Import-Module ActiveDirectory


####### Nye OU`s #########

New-ADOrganizationalUnit "On-PremiumIT" -Description "On-PremiumIT OU inneholder OUer"

New-ADOrganizationalUnit "Computere" -Description "Inneholder firmaers servere og arbeidsstasjoner" -Path "OU=On-PremiumIT,DC=tafi,DC=sec"
New-ADOrganizationalUnit "Servere" -Description "Inneholder firmaets servere" -Path "OU=Computere,OU=On-PremiumIT,DC=tafi,DC=sec"
New-ADOrganizationalUnit "Arbeidsstasjoner" -Description "Inneholder firmaets arbeidsstasjoner" -Path "OU=Computere,OU=On-PremiumIT,DC=tafi,DC=sec"

New-ADOrganizationalUnit "Users" -Description "Inneholder firmaers brukere" -Path "OU=On-PremiumIT,DC=tafi,DC=sec"
New-ADOrganizationalUnit "IT-drift" -Description "IT-drift OU" -Path "OU=Users,OU=On-PremiumIT,DC=tafi,DC=sec"
New-ADOrganizationalUnit "Developer Team" -Description "Developer Team OU" -Path "OU=Users,OU=On-PremiumIT,DC=tafi,DC=sec"
New-ADOrganizationalUnit "Regnskap" -Description "Regnskap OU" -Path "OU=Users,OU=On-PremiumIT,DC=tafi,DC=sec"
New-ADOrganizationalUnit "Renhold" -Description "Rehold OU" -Path "OU=Users,OU=On-PremiumIT,DC=tafi,DC=sec"
New-ADOrganizationalUnit "Human resources" -Description "Human resources OU" -Path "OU=Users,OU=On-PremiumIT,DC=tafi,DC=sec"

New-ADOrganizationalUnit "Ressurser" -Description "Inneholder firmaets ressurser" -Path "OU=On-PremiumIT,DC=tafi,DC=sec"

Get-ADComputer "TAFI-SRV1" | Move-ADObject -TargetPath "OU=Servere,OU=Computere,OU=On-PremiumIT,DC=tafi,DC=sec"
Get-ADComputer "TAFI-CLI" | Move-ADObject -TargetPath "OU=Arbeidsstasjoner,OU=Computere,OU=On-PremiumIT,DC=tafi,DC=sec"


## Her lager vi en global security group for alle avdelingene
$PathGroups = "OU=Users,OU=On-PremiumIT,DC=tafi,DC=sec"
$grupper=@("IT-drift", "Developer Team", "Regnskap", "Renhold", "Human resources")
$sam=@("it.admins", "developers", "regnskap", "renhold", "hr")

$grupper | ForEach-Object {New-ADGroup -GroupCategory Security -GroupScope Global -name "g_$_" -Path "OU=$_,$PathGroups"-SamAccountName $sam[$grupper.IndexOf($_)]} 


## Her lager vi lokale sikkerhetsgrupper for ressursene
$PathRessurser = "OU=Ressurser,OU=On-PremiumIT,DC=tafi,DC=sec"
$ressurser=@("printere", "adgangskort")

$ressurser | ForEach-Object {New-ADGroup -name $_ -Path $PathRessurser -GroupCategory Security -GroupScope DomainLocal }


## Her legger vi de globale gruppene inn i lokale grupper slik at de får tilgang til ressursene

Add-ADGroupMember -identity "printere" -Members "renhold","it.admins","developers","regnskap","hr"
Add-ADGroupMember -identity "adgangskort" -Members "renhold","it.admins","developers","regnskap","hr"

## Her legger vi til IT admins i Domain Admins slik at IT-Drift får fulle rettigheter til domenet

$adminGroups = @("Domain Admins", "Domain Controllers", "Administrators", "Enterprise Admins", "Group Policy Creator Owners", "Schema Admins")
$adminGroups | ForEach-Object { Add-ADGroupMember -Identity $_ -Members "it.admins" }


## Oppretter felles datamaskiner

for ($i=1;$i -lt 5;$i++) {
    New-ADComputer -Name "FellesPC$i" -Path 'OU=Arbeidsstasjoner,OU=Computere,OU=On-PremiumIT,DC=tafi,DC=sec' -Location "$i. etasje" -ManagedBy "it.admins"
    }
## Gir alle ansatte tilgang til computere som har blitt med i domenet

    Add-ADGroupMember -identity "Domain Computers" -Members "renhold","it.admins","developers","regnskap","hr"










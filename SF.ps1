# Koden her må kjøres på serveren 


# Installasjon av avhengihetene
$instalstate = (Get-WindowsFeature -Name FS-DFS-Namespace).Installstate

if ($instalstate -match "Available") {
    Install-WindowsFeature -Name FS-DFS-Namespace,FS-DFS-Replication,RSAT-DFS-Mgmt-Con -IncludeManagementTools
}

Import-Module dfsn

# Laging av folderne og deler folderne i netverket (\\tafi.srv1)
$Foldernames = @('SF_IT-Drift', 'SF_Developer Team', 'SF_Regnskap', 'SF_Renhold', 'SF_Human resources',"files")

$rootpath = "C:\On-PremiumIT-files"
$Foldernames | ForEach-Object {New-Item "$rootpath\$_" -Type Directory}

$Foldernames | ForEach-Object {New-SmbShare -name "$_" -path "$rootpath\$_" -FullAccess Everyone};


# Innstall av en DFS-Root, og legger de delte mappene i den lagede DFS-en.
New-DfsnRoot -TargetPath \\tafi-srv1\files -Path \\tafi.sec\file -Type DomainV2
$Foldernames | Where-Object {$_ -like "*SF_*"} | ForEach-Object {$DfsPath = (‘\\tafi.sec\file\’ + $_); $targetPath = (‘\\tafi-srv1\’ + $_);New-DfsnFolderTarget -Path $dfsPath -TargetPath $targetPath}

## ACL permissions 
# Her legges det permissions for hver av de delte mappene i DFS-en, og fjerne tilgang fra de locale brukere (Builtin\users).

$sam = @("it.admins", "developers", "regnskap", "renhold", "hr")


$Foldernames | Where-Object {$_ -like "*SF_*"} | ForEach-Object {

$sammed = $sam[$Foldernames.IndexOf($_)]

$acl = Get-Acl \\tafi.sec\file\$_
$AccessRule = New-Object System.Security.AccessControl.FileSystemAccessRule( "tafi\$sammed","FullControl","Allow")
$acl.SetAccessRule($AccessRule)
$ACL | Set-Acl -Path "\\tafi.sec\file\$_"

$ACL = Get-Acl -Path "\\tafi\file\$_"
$ACL.SetAccessRuleProtection($true,$true)
$ACL | Set-Acl -Path "\\tafi\file\$_"

$acl = Get-Acl "\\tafi\file\$_"
$acl.Access | Where-Object {$_.IdentityReference -eq "BUILTIN\Users" } | ForEach-Object { $acl.RemoveAccessRuleSpecific($_) }
Set-Acl "\\tafi\file\$_" $acl
   
}

# Mappeoppgave 2 - Active Directory
 
 
## Case-beskrivelse
Målet med dette prosjektet er å sette opp en AD-infrastruktur for konsulentfirmaet On-PremiumIT. Bedriften er delt opp inn i fem avdelinger hvor hver av avdelingene skal ha egne rettigheter og tilganger til ressurser. Spesifikk informasjon angående krav finnes i oppgaveteksten.
 
## AD-struktur

![alt text](https://media.discordapp.net/attachments/813776799325618196/822137240125177856/AD_Struktur.jpg?width=939&height=669 "AD struktur")

AD-en er strukturert inn i tre hoved-OUer, Users, Computere og Ressurser. Users består av ytterligere OUer som hver representerer en avdeling i bedriften. Computers består av to OUer som er servere og arbeidsstasjoner. Ressurserer består videre av to undergrupper som brukes for å gi permissions til printere og adgangskort.

Users-OUen består av under-OUer som inneholder brukerobjekter og grupper. Det samme gjelder for Computere bare her er det kun maskiner. Grunnen for dette designet er fleksibilitet og skalerbarhet. Lokale kontoer er fjernet slik at alle kontoer som kan brukes for å logge inn i maskinene er under domenet. Dermed trengs det ikke å resette en datamaskin når den overgis til en annen person.

### Powershell kode

AD.ps1 koden starter med å installere verktøy som er nødvendig for å få strukturen på plass(Active Directory) 
Så kommer koden for å opprette OU-strukturen i domenet og gruppestrukturen. 
Deretter delegeres rettigheter vha. grupper. 

## GPO
 
### Designavgjørelser
 
Vi har tatt en rekke designavgjørelser rettet mot oppsettet og fordelingen av GPOer. De er følgende:
*	Ikke linke GPO i domenenivå
*	Linke GPO til øverste mulige OU root nivå
*	Mindre GPOer

Disse designavgjørelsene sørger for en god organisering av GPOer slik at videre påbygging av den eksisterende strukturen er relativt greit å gjennomføre. I tillegg, ved å minske størrelsene på hver GPO så fører det til en mer effektiv troubleshooting ved konflikt i systemet.

Det er viktig å notere at navnet til .ps1-filene med kode for installasjon av GPOer samsvarer med strukturen til GPOene.

### GPO-struktur

![alt text](https://media.discordapp.net/attachments/813776799325618196/822154776253956136/image0.png?width=583&height=676 "AD struktur")

GPOene er strukturert ut i fra OUene, hvor hovedgruppene er Users og Computere. OUene i Users kategoriseres enten som "Restricted Access", som består av HR, renhold og regnskap, eller "Full Access", som består av developers og IT-drift. Denne kategoriseringen fordeler de ulike avdelingene utifra tilgangsnivået til datamaskinene.


### Users

I Users er det linket GPOer som skal gjelde universalt for alle brukere. Den viktigste GPOen som linkes her sørger for at skjermsparer aktiveres etter 5 minutters inaktivitet. Dette er veldig enkel regel som også er svært effektiv og vil hindre mange potensielle angrep. Videre GPOer som skal gjelde for alle users kan bestemmes ut i fra bedriftens egne ønsker.

**Restricted Access**

Restricted Access består av OUene til HR, renhold og regnkap. Her linkes det GPOer som begrenser deres rettigehter og dermed styrker sikkerheten til hele domenet. De har ikke tilgang til kontroll panel, terminaler, registry editor og nektes å kjøre .EXE filer, alt som kan føre til potensielle sikkerhetshull. Ideen er at brukere tilhørende disse avdelingene ikke har bruk for avanserte programmer og funksjoner, og derfor kan PCene deres begrenses. I tillegg skal all nødvendig software de skal bruke være ferdiginstallert ved overgivelse av PC.

**Full Access**

Full Access består av developer og IT-drift OUene. Disse to bruker-OUene trenger større frihet fra restriksjoner for å effektivt kunne gjøre jobbene sine. Begge avdelingene har full lokal administratortilgang til maskinene sine. Forskjellen er at IT-drift brukere også vil ha tilgang til selve infrastrukturen og domenet mens developer er isolert i sin egen OU. Alle brukere i IT-drift er medlem av den innebygde admingruppen til domenet, mens brukere i developer kun er vanlige domenebrukere, uten noen privileger.


### Computere

OUen Computere består av servere og arbeidsstasjoner, hvor servere er en generell OU for infrastrukturen og arbeidsstasjoner er alle datamaskiner tilrettelagt bruk av de ansatte. Det er implementert flere GPOer som skal sørge for økt sikkerhet.

* Anonymous SID enumeration er slått av så man ikke kan få tak i brukernavn gjennom Security ID.
* Alle lokale gjestebrukere fjernet.
* Lagt til passordkrav og utløpelsestid på 6 måneder for hver passordene.
* Tvungen restart av system pga oppdateringer er slått av.
* Onedrive er slått av.
* Alle innlogginger blir loggført.
* Alle removable disks (USb minnepinne, osv.) kan ikke execute.

Disse GPOene vil sørge for god sikkerhet og pålitelighet for både infrastrukturen og arbeidsmaskinene. Alle forsøk på innlogginger blir loggført, og i tillegg er potensialet for angrep via eksterne disker eller lagringsmedier over cloud minsket. Mer ekstreme tiltak kan gjennomføres på grupper med mer sensitive maskiner, men disse tiltakene er universale for alle datamaskiner.

**Servere**

Serverene har kun en essensiell GPO linket til seg. Denne GPOen tillater RDP oppkoblinger til serveren, og kun fra medlemmer tilhørende domeneadministratorer gruppen. Siden kun brukere i IT-drift er domeneadministratorer, sørger dette for at kun de har rettigheter til å koble seg til infrastrukturen. Alle andre brukere som forsøker oppkobling blir nektet tilgang.

**Arbeidsstasjoner**

En mulig GPO for datamaskiner i arbeidsstasjoner er å deaktivere windows installer. Dette vil hindre innstalleringen av all software, men medfører en ulempe i form av manglende brukerfrihet. Derfor er dette kun en potensiell GPO som kun bør implementeres i visse datasystemer som inneholder mer sensetiv informasjon.


### Powershell kode

Powershell koden som installerer GPOene må delvis gjennomføres manuelt i GUI. Det er lagt til en fil, How-To-Create-GPO.ps1, som beskriver prosessen av å lage og implementere nye GPOer. Alle kommandoene i GPO-filene må utføres manuelt, linje for linje, for å være suksessfulle. I tilegg er navnene på filene relatert til hvilket OU GPOene linkes til.



## Sikkerhetsgrupper
 
 Vi tar i bruk lokale sikkerhetsgrupper når det kommer til lokale ressurser som printere og adgangskort, som ligger i samme domene som gruppen.

 Vi opprettet globale sikkerhetsgrupper for alle avdelinger og plasserte gruppene inn i avdelingens OU. Det er hensiktsmessig å at alle ansatte i en avdeling blir lagt i en global sikkerhetsgruppe fordi de deler applikasjoner, shared folders etc. og de kan tilegnes andre lokale grupper som feks. printere og adgangskort.
 
 For at ansatte skulle få tilgang til ressursene opprettet vi lokale sikkerhetsgrupper som representerer adgangskort og printere og knyttet avdelingenes globale sikkerhetsgrupper til disse. 

 Vi meldte it.admins gruppen inn i Domain Admins, Domain Controllers, Administrators, Enterprise Admins, Group Policy Creator Owners og Schema Admins slik at it.admins arver rettighetene fra disse grupene og får fulle rettigheter til domenet.

 
## Server

Filen IIS Install.ps1 må kjøres på server. Den installerer avhengigheter, deretter IIS på serveren, så settes en HTML template opp som default homepage 

Vha en GPO blir denne siden gjort til default homepage for alle Users ved å tilknytte GPOen til Users OU.

Vi laget et eget script for Overvåkning av SRV 1. Dette scriptet henter ut CPU, Availible memory, prosesser, IOPS, memory availible bytes, network adapter transfer og USB transfer(dersom USB er tilkoblet). Scrptet tar opp dette i 10 sekunder, skriver det til en CSV fil og genererer en grafisk representasjon av ressursbruken. 
Dette blir lagret i en shared folder som kun it.admins har tilgang til. 

Tilkoblingen til serveren blir gjort med `Invoke-Command` som kjører tilkoblingen med SSH.

Filen SF.ps1 må kjøres på server. Den lager delt filområde for alle avdelinger på domenet og bruker grupper for å gi tilgang til mappene, Hver gruppe har kun tilgang til sin avdelings delte mappe, untatt it.admins som har tilgang alle delte mapper. Alle grupper har tilgang til DFS-en dersom noe skal deles på tvers av avdelinger.

## Brukeropprettelse

Vi valgte å bruke `Read-Host` for brukeropprettelse siden dette gjør det lettere for en administrator å opprette brukere og ungår brukernavnkonflikt. 

Administratoren trenger kun å kjøre scriptet createUsers.ps1, velge hvor mange brukere han vil opprette, skrive inn fornavn og etternavn til brukeren, og å velge hvilken avdeling brukeren skal tilhøre for at brukeren skal bli opprettet. 

Fra denne informasjonen oppretter scriptet brukernavn, epost, og et standardpassord som brukeren må endre ved første pålogging. Så blir brukeren lagt i avdelingens OU og lagt til som medlem av avdelingens gruppe. Etter det er gjort så blir informasjonen eksportrt til en CSV fil, slik at informasjonen skal bli lagret. 

Om bedriften må sette opp strukturen på nytt, så har vi laget et separat script CSVcreate.ps1 som oppretter alle brukerene fra denne CSV-filen. 

Vi har også lagt til et lite script hvor administrator kan liste ut så mange log events med ID 4720 han ønsker for å teste at brukeropprettelsen stemmer overens med det faktiske antallet brukere som er opprettet.

## Kilder 
Kilder som ble brukt er microsoft docs, forelesninger og penusm bøkene.


## Git repository
[lenken til git repository](https://gitlab.stud.iie.ntnu.no/jorgentn/mappeoppgave-2-active-directory-dcst1005)

 
 
 
 


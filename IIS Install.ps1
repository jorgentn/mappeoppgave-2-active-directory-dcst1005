# Filen her må kjøres på serveren

# Installere Choco dersom den ikke er installert
if (!(Test-Path "C:\ProgramData\chocolatey\choco.exe")) {
    Write-Output "Installing Chocolatey.....";
    Set-ExecutionPolicy Bypass -Scope Process -Force; Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
}

choco install 7zip.install

# Nelastingen av templates og flytting av en template
$source = 'https://www.free-css.com/assets/files/free-css-templates/download/page264/rocket-internet.zip'

$destination = "C:\Users\$env:username\Downloads\rocket-internet.zip"

Invoke-WebRequest -Uri $source -OutFile $destination
7z x $destination -o"C:\Users\$env:username\Downloads\folder"
Copy-Item -Path "C:\Users\$env:username\Downloads\folder\*" -Destination \\tafi.sec\file -recurse -Force

# Install av IIS
Install-WindowsFeature -name Web-Server -IncludeManagementTools

# Kopi templaten til root mappen for å få templaten til å kjøre i IIS.
Copy-Item -Path \\tafi.sec\file\rocket-internet\* -Destination "C:\inetpub\wwwroot" -recurse -Force

### GPO for IIS ligger i GPO mappen siden den må kjøres i DC


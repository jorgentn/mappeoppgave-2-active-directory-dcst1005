    # NO REMOVABLE DISKS FOR RESTRICED USERS

$GPOName = 'No Removable Disks - User Level'
$Comment = 'Hindrer at restricted users kan read eller write til en removable disk'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg enable
<#
    User Configuration > Policies > Administrative Templates > System > Removable Storage Access > All Removable Storage classes: Deny all access
#>

# Linker GPOen til HR, regnskap og renhold
$OU = 'OU=Human Resources,OU=Users', 'OU=Regnskap,OU=Users', 'OU=Renhold,OU=Users'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,DC=tafi,DC=sec"
}



    # HIDE PARTITION IN DISKS

$GPOName = 'Hide Partition'
$Comment = 'Skjuler partition i disker'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg partitionene du vil skjule. Det er lurt med navnsystemer på skjulte partition for bedriftens PCer for at denne GPOen skal være effektiv.
<#
    User Configuration > Policies > Administrative Templates > Windows Components > File Explorer > Hide these specified drives in My Computer

#>

# Linker GPOen til HR, regnskap og renhold
$OU = 'OU=Human Resources,OU=Users', 'OU=Regnskap,OU=Users', 'OU=Renhold,OU=Users'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,DC=tafi,DC=sec"
}



    # RESTRICT ACCESS TO CONTROL PANEL

$GPOName = 'Disable Control Panel'
$Comment = 'Fjerner tilgang til control panel'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg enable
<#
    User Configuration > Policies > Administrative Templates > Control Panel > Prohibit access to the control panel

#>

# Linker GPOen til HR, regnskap og renhold
$OU = 'OU=Human resources,OU=Users', 'OU=Regnskap,OU=Users', 'OU=Renhold,OU=Users'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,DC=tafi,DC=sec"
}



    # RESTRICT ACCESS TO TERMINALS

$GPOName = 'Disable Terminal'
$Comment = 'Fjerner tilgang til terminaler'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg enable
<#
    User Configuration > Policies > Administrative Templates > System > Prevent access to the command prompt

#>

# Linker GPOen til HR, regnskap og renhold
$OU = 'OU=Human Resources,OU=Users', 'OU=Regnskap,OU=Users', 'OU=Renhold,OU=Users'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,DC=tafi,DC=sec"
}



    # SOFTWARE RESTRICTION       

$GPOName = 'Software Restrictions'
$Comment = 'Setter opp software restrictions til brukere i restricted groups'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende mappe
<#
    User Configuration > Policies > Windows Settings > Security Settings > Software Restriction Policies

    Her kan man konfigurerer regler til restricted users angående bruk av software. 
        Dette gjelder blant annet å restricte visse applikasjoner eller kun tilate fra spesifikke publishers.
#>

# Linker GPOen til HR, regnskap og renhold
$OU = 'OU=Human Resources,OU=Users', 'OU=Regnskap,OU=Users', 'OU=Renhold,OU=Users'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,DC=tafi,DC=sec"
}



    # NO WINDOWS REGISTRY EDIT

$GPOName = 'Disable Windows Registry Edit'
$Comment = 'Fjerner mulighet for å endre på windows registry'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg enable
<#
    User Configuration > Policies > Administrative Templates > System > Prevent access to registry editing tools
#>

# Linker GPOen til HR, regnskap og renhold
$OU = 'OU=Human Resources,OU=Users', 'OU=Regnskap,OU=Users', 'OU=Renhold,OU=Users'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,DC=tafi,DC=sec"
}




    # DISABLE RUNNING .EXE FILES

$GPOName = 'Disable running .exe files'
$Comment = 'Disable running .exe files'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg de ønskede innstillingene
<#
    User Configuration > Policies > Windows Settings > Seciurty Settings > Software Restriction Policies > Designated file types > Properties > select exe extension > Remove it > Apply and OK
#>

$OU = 'OU=Human Resources,OU=Users', 'OU=Regnskap,OU=Users', 'OU=Renhold,OU=Users'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,DC=tafi,DC=sec"
}


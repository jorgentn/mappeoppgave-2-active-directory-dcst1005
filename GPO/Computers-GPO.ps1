    # NO FORCED SYSTEM RESTART

$GPOName = 'No Forced System Restart'
$Comment = 'Hindrer tvungen restart av pc pga oppdatering, osv.'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg enable
<#
    Computer Configuration > Administrator Templates > Windows Components > Windows Update > No auto-restart with logged on users for scheduled automatic update installations
#>

# Linker GPOen til Computere
$OU = 'OU=Computere'
Get-GPO -Name $GPOName | 
    New-GPLink -Target "$OU,DC=tafi,DC=sec"




    # NO LOCAL GUEST ACCOUNT

$GPOName = 'No Local Guest Account'
$Comment = 'Fjerner lokale gjestekontoer på PCen'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg enable
<#
    Computer Configurate > Windows Settings > Security Settings > Local Policies > Security Options > Network cccess: Allow anonymous SID/Name translation
#>

# Linker GPOen til Computere
$OU = 'OU=Computere'
Get-GPO -Name $GPOName | 
    New-GPLink -Target "$OU,DC=tafi,DC=sec"





    # DISABLE ANONYMOUS SID ENUMERATION

$GPOName = 'No SID Enumeration'
$Comment = 'Hindrer muligheten for å få tak i brukernavn ved å bruke en brukes SID'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg disable
<#
    Computer Configurate > Windows Settings > Security Settings > Local Policies > Security Options > Network cccess: Allow anonymous SID/Name translation
#>

# Linker GPOen til Computere
$OU = 'OU=Computere'
Get-GPO -Name $GPOName | 
    New-GPLink -Target "$OU,DC=tafi,DC=sec"





    # TRACK ACCOUNT LOGINS

$GPOName = 'Track Account Logins'
$Comment = 'Tracks all attempted and successfull logins on the computer'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg enable
<#
    Computer Configuration > Windows Settings > Security Settings > Local Policies > Audit Policy > Audit logon events
#>

# Linker GPOen til Computere
$OU = 'OU=Computere'
Get-GPO -Name $GPOName | 
    New-GPLink -Target "$OU,DC=tafi,DC=sec"





    # DISABLE ONEDRIVE

$GPOName = 'Disable Onedrive'
$Comment = 'Fjerner Onedrive fra PCen'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg enable
<#
    Computer Configuration > Policies > Administrative Templates > Windows Components > OneDrive > Prevent usage of OneDrive for file storage
#>

# Linker GPOen til Computere
$OU = 'OU=Computere'
Get-GPO -Name $GPOName | 
    New-GPLink -Target "$OU,DC=tafi,DC=sec"





    # PASSWORD REQUIREMENTS

$GPOName = 'Password Requirements'
$Comment = 'Setter krav for passord'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og gjør som vist nedenfor
<#
    Computer Configuration > Policies > Windows Settings > Security Settings > Account Policies > Password Policies

    Enable deretter følgende:
        > Maximum Password Age
            Begrenser maks alder for et passord. Vi gjør for 6 måneder

        > Password must meet complexity requirements
            Passord må møte en rekke krav for å være gyldige

        > Minimum password length
            Passord må ha en minimum lengde for å være gyldige
#>

# Linker GPOen til Computere
$OU = 'OU=Computere'
Get-GPO -Name $GPOName | 
    New-GPLink -Target "$OU,DC=tafi,DC=sec"





    #CAN NOT EXECUTE REMOVEABLE DISKS

$GPOName = 'No Execute Removeable Disks'
$Comment = 'Kan ikke utføre flyttbare disker'
New-GPO -Name $GPOName -Comment $Comment

<#
Computer Configuration > Policies > Administrative Templates > System > Removable Storage Access > Removable Disks: Deny execute access
#>
# Kun tilgjengelig på machine level. User level lar deg kun nekte read- og write-access.

# Linker GPOen til arbeidsstasjoner
$OU = 'OU=Arbeidsstasjoner,OU=Computere'
Get-GPO -Name $GPOName | 
    New-GPLink -Target "$OU,DC=tafi,DC=sec"

    

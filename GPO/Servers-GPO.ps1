    # RDP TO SERVERS      

$GPOName = 'Allow RDP'
$Comment = 'Tillatt kun domain admins til å kjøre RDP til serverne'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg enable
<#
    Computer Configuration > Policies > Windows settings > Security Settings > Local policies > User Rights Assignment > Allow log on through Remote Desktop Services

    Trykk "Add User or Group" og velg domeneadmin gruppen. IT-drift OUen er med i domeneAdmin, og får derfor rdp-tilgang til infrastrukturen.
#> 

# Linker GPOen til RDP
$OU = 'OU=Servere','OU=Computere'
Get-GPO -Name $GPOName | 
    New-GPLink -Target "OU=$OU,DC=tafi,DC=sec"



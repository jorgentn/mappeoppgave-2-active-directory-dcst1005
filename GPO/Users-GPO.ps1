    # START MENU OG TASKBAR CUSTOM SETTINGS

$GPOName = 'Taskbar and Start Menu GPO'
$Comment = 'Generelle GPOer relatert til taskbar og start menu'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg de ønskede innstillingene
<#
    User Configuration > Policies > Administrative Templates > Start Menu and Taskbar
#>

# Linker GPOen til Users
$OU = 'Users'
Get-GPO -Name $GPOName | 
    New-GPLink -Target "OU=$OU,DC=tafi,DC=sec"



    # FORCE SCREEN SAVER

$GPOName = 'Force Screen Saver'
$Comment = 'Starter screen saver etter 5 minutter med innaktivitet'
New-GPO -Name $GPOName -Comment $Comment

# Gå til følgende innstilling og velg de ønskede innstillingene
<#
    User Configuration > Policies > Administrative Templates > Control Panel > Personalization > Screen saver timeout

    Velg enable og skriv inn 300 sekunder.
#>

# Linker GPOen til Users
$OU = 'Users'
Get-GPO -Name $GPOName | 
    New-GPLink -Target "OU=$OU,DC=tafi,DC=sec"




# Lager en ny GPO i domenet
$GPOName = 'Min GPO'
$Comment = 'Dette er en kommentar'
New-GPO -Name $GPOName -Comment $Comment

# Gå inne i Group Policy Managment og finn GPOen du lagde med kommandoen under
gpmc.msc
# Høyretrykk og velg edit på GPOen

# Konfigurer GPO ved å velge en innstilling og konfigurere den

# Link GPOen til en OU slik at den gjelder for alle objekter i OUen
$OU = 'OU=Min-OU'
Get-GPO -Name $GPOName | 
    New-GPLink -Target "$OU,DC=domain,DC=sec"

# Eller om man har flere OU, man skal linke ti

$OU = 'OU=OU1', 'OU=OU2', 'OU=OU3'
foreach ($item in $OU) {
    Get-GPO -Name $GPOName | New-GPLink -Target "$item,DC=domain,DC=sec"
}

# Man kan kopiere GPOer til en annen lokasjon om man trenger en sandbox-environment for å endre på GPOen
$GPOName = 'Min GPO'
$TargetName = 'Min GPO Kopi'
$TargetDomain = 'andre.domene'
Copy-GPO -SourceName $GPOName -TargetName $TargetName -TargetDomain $TargetDomain

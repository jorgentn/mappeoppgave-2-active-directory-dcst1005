    #DISABLE WINDOWS INSTALLER
    
$GPOName = 'Disable windows installer'
$Comment = 'Disable windows installer'
New-GPO -Name $GPOName -Comment $Comment

<#
    Computer Configuration > Administrative Templates > Windows Components > Windows Installer > Disable Windows Installer

    Dette trenger nødvendigvis ikke å gjøres på alle PCer. Kun PCer som ikke trenger ytterligere software installert.
#>

# Linker GPOen til arbeidsstasjoner
$OU = 'OU=Arbeidsstasjoner,OU=Computere'
Get-GPO -Name $GPOName | 
    New-GPLink -Target "$OU,DC=tafi,DC=sec"





